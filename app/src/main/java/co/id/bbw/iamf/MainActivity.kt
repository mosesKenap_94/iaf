package co.id.bbw.iamf

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.RemoteViews
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.res.ResourcesCompat
import co.id.bbw.iamf.beans.NotifList
import co.id.bbw.iamf.utils.ImagePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.util.*

const val RC_PERM = 101
const val CHANNEL_ID = "IAF"
const val CHANNEL_NAME = "IAF_MESSAGE"
const val RC_PICK_IMG = 102

open class MainActivity : BaseActivity(), EasyPermissions.PermissionCallbacks {
    private var notificationManager: NotificationManager?=null
    private var selectedFontStyle = 0
    private var selectedFontType = 0
    private var selectedFontSize = 0
    private var setPeriodic = 0
    private var imgFile: File? = null


    //for edit purposes
    private var isEdit:Boolean = false
    var editIdNotif:Int?=null
    var editImg:String?=null

    override fun setLayout(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appbar = supportActionBar
        appbar?.setDisplayHomeAsUpEnabled(true)
        appbar?.setDisplayShowTitleEnabled(true)

        if(!hasPermissionWriteExternalStorage()){
            EasyPermissions.requestPermissions(
                this,
                "Application need this permission to access feature",
                RC_PERM,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }

        isEdit = intent.getBooleanExtra("edit", false)
        if(isEdit){
            //edit exist notification
            appbar?.title = "Edit"
            editIdNotif = intent.getIntExtra("id", 0)
            checkExistNotif(editIdNotif)
        }else{
            //create new notification
            appbar?.title = "Create Notification"
            etTitle.setText(getString(R.string.iaf))
            oneTime.isChecked = true
        }
        rgOption.setOnCheckedChangeListener { group, checkedId ->
            if(checkedId == R.id.oneTime){
                lySetTime.visibility = View.GONE
            }else if(checkedId == R.id.periodic){
                lySetTime.visibility = View.VISIBLE
            }
        }

        addImage.setOnClickListener {
            if(imgFile != null){
                MaterialAlertDialogBuilder(this)
                    .setTitle("Alert")
                    .setMessage("Do you want to remove image or change image?")
                    .setPositiveButton("Remove"){ dialof, _ ->
                        dialof.dismiss()
                        removeImage()
                    }
                    .setNegativeButton("Change Image"){ dialof, _ ->
                        dialof.dismiss()
                        val chooseImage = ImagePicker.getPickPostIntent(this, 0)
                        startActivityForResult(chooseImage, RC_PICK_IMG)
                    }
                    .create()
                    .show()
            }else {

                /**
                 * val pickIntent = Intent(Intent.ACTION_PICK)
                    pickIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/")
                    val resInfo: List<ResolveInfo> = packageManager.queryIntentActivities(pickIntent, 0)
                    for (resolveInfo in resInfo) {
                    Log.e("packageName", resolveInfo.activityInfo.packageName)
                    }
                    if(pickIntent.resolveActivity(packageManager) != null)
                        startActivityForResult(pickIntent, RC_PICK_IMG)
                    else
                        openFileGeneral()
                 */
                val chooseImage = ImagePicker.getPickPostIntent(this, 0)
                if(chooseImage.resolveActivity(packageManager) != null)
                    startActivityForResult(chooseImage, RC_PICK_IMG)
                else
                    Toast.makeText(this, "device has no app to handle gallery intent", Toast.LENGTH_SHORT).show()
            }
        }

        notificationManager =getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.lightColor = Color.GREEN
            channel.importance = NotificationManager.IMPORTANCE_HIGH
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager?.createNotificationChannel(channel)
        }

        spStyle.setSelection(selectedFontStyle)
        spType.setSelection(selectedFontType)
        spFontSize.setSelection(selectedFontSize)
        spStyle.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedFontStyle = position
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedFontType = position
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spFontSize.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedFontSize = position
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        etDesc.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                countText.text = s!!.toString().length.toString()
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        submit.setOnClickListener {
            if(periodic.isChecked){
                if(timePeriodicly.text.toString().isEmpty()){
                    timePeriodicly.error = "Please fill this field"
                    return@setOnClickListener
                }
            }

            if(!oneTime.isChecked){
                setPeriodic = timePeriodicly.text.toString().toInt() * 1000
            }

            if(isEdit){
                updateNotification()
            }else{
                realm.executeTransaction {
                    val currentId = realm.where(NotifList::class.java).max("idNotif")
                    var newId = 0
                    if (currentId == null)
                        newId=1
                    else
                        newId = currentId.toInt() + 1

                    val notif = NotifList()
                    notif.idNotif = newId
                    notif.title = etTitle.text.toString()
                    notif.desc = etDesc.text.toString()
                    notif.descFontStyle = selectedFontStyle
                    notif.descFontSize = selectedFontSize
                    notif.descFontType  = selectedFontType

                    if(imgFile != null){
                        notif.img = imgFile!!.path
                    }
                    notif.duration = setPeriodic

                    if(oneTime.isChecked) {
                        notif.isNotPeriodic = true
                        callnotify(newId)
                    }else if(periodic.isChecked){
                        notif.isNotPeriodic = false
                        val periodicTime = timePeriodicly.text.toString().toInt() * 1000
                        periodicNotif(periodicTime, etDesc.text.toString().trim(), newId)
                    }

                    it.insertOrUpdate(notif)
                }
            }

            finish()
        }

    }

    private fun removeImage() {
        imgFile = null
        addImage.setImageResource(R.drawable.bg_add_img)
    }

    private fun updateNotification() {
        realm.beginTransaction()

        val curData = realm.where(NotifList::class.java).equalTo("idNotif", editIdNotif).findFirst()

        curData?.img = imgFile?.path?:"no_image"
        curData?.title = etTitle.text.toString()
        curData?.desc = etDesc.text.toString()
        curData?.duration = setPeriodic
        curData?.descFontSize = selectedFontSize
        curData?.descFontType = selectedFontType
        curData?.descFontStyle = selectedFontStyle

        if(oneTime.isChecked) {
            curData?.isNotPeriodic = true
            callnotify(editIdNotif!!)
        }else if(periodic.isChecked){
            curData?.isNotPeriodic = false
            val periodicTime = timePeriodicly.text.toString().toInt() * 1000
            periodicNotif(periodicTime, etDesc.text.toString().trim(), editIdNotif!!)
        }

        realm.commitTransaction()

    }

    private fun checkExistNotif(editIdNotif: Int?) {
        realm.beginTransaction()

        val curData = realm.where(NotifList::class.java).equalTo("idNotif", editIdNotif).findFirst()

        editImg = curData?.img?:""
        etTitle.setText(curData?.title ?: "")
        etDesc.setText(curData?.desc ?: "")
        selectedFontType = curData?.descFontType?:0
        selectedFontStyle = curData?.descFontStyle?:0
        selectedFontSize = curData?.descFontSize?:0


        if(editImg!!.isNotEmpty()){
            val tempFileImg = File(editImg)
            imgFile = tempFileImg
            addImage.setImageBitmap(BitmapFactory.decodeFile(imgFile!!.path))
        }

        if(curData!!.isNotPeriodic!!){
            oneTime.isChecked = true
            periodic.isChecked = false
            lySetTime.visibility = View.GONE
        }else{
            periodic.isChecked = true
            oneTime.isChecked = false
            lySetTime.visibility = View.VISIBLE
        }

        if(curData.duration != 0){
            timePeriodicly.setText(((curData.duration!!) / 1000).toString())
            setPeriodic = curData.duration!!
        }else{
            timePeriodicly.setText("")
            setPeriodic = 0
        }

        submit.text = "Update"

        realm.commitTransaction()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_PICK_IMG){
                //new way
                val mimeType = contentResolver.getType(data!!.data!!)
                if(mimeType!!.contains("image")){
                    //true
                    val imgFilePath = ImagePicker.getImageFromResult(this, resultCode, data)
                    val tempFileImg = File(imgFilePath)
                    if(ImageUtils.isSizeLessThan_2MB(tempFileImg.length())){
                        imgFile = tempFileImg
                    }else{
                        imgFile = ImageUtils.compressImage(this, tempFileImg)
                    }
                    addImage.setImageBitmap(BitmapFactory.decodeFile(imgFile!!.path))
                }else{
                    //false
                    Toast.makeText(this, "Please add image JPEG/PNG", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    fun periodicNotif(duration: Int, msg: String, newId: Int){

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, BootRecieverNotif::class.java)
        intent.putExtra("duration", duration)
        intent.putExtra("message", msg)
        intent.putExtra("img", imgFile?.path)
        intent.putExtra("idNotif", newId)
        intent.putExtra("title", etTitle.text.toString())
        intent.putExtra("fontstyle", selectedFontStyle)
        intent.putExtra("fonttype", selectedFontType)
        intent.putExtra("fontsize", selectedFontSize)

        val pendingIntent = PendingIntent.getBroadcast(
            this,
            newId,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val calendar = Calendar.getInstance(Locale("id", "ID"))
        calendar.set(Calendar.MILLISECOND, duration)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                pendingIntent
            )
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
        }
    }

    fun callnotify(id: Int) {

        val normalNotif = RemoteViews(packageName, R.layout.custom_notification)
        val expandedNotif = RemoteViews(packageName, R.layout.custom_notification_expanded)

        val msg = if(etDesc.text.toString().isEmpty()) "No messages added" else etDesc.text.toString()
        val spancontenttext = SpannableString(msg)
        val textSizeVal = when(selectedFontSize){
            1 -> 14
            2 -> 16
            3 -> 18
            4 -> 20
            5 -> 24
            else -> 12
        }
        val typefaceFontStyle = when(selectedFontStyle){
            1 -> ResourcesCompat.getFont(this, R.font.times_new_roman)
            2 -> ResourcesCompat.getFont(this, R.font.calibri)
            else -> Typeface.DEFAULT
        }
        val typefaceStyle = when(selectedFontType){
            1 -> Typeface.BOLD
            2 -> Typeface.ITALIC
            else -> Typeface.NORMAL
        }
        spancontenttext.setSpan(
            AbsoluteSizeSpan(textSizeVal, true),
            0,
            msg.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
        spancontenttext.setSpan(
            StyleSpan(typefaceFontStyle!!.style),
            0,
            msg.length,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        spancontenttext.setSpan(
            StyleSpan(typefaceStyle),
            0,
            msg.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        normalNotif.setTextViewText(R.id.title, etTitle.text.toString())
        normalNotif.setTextViewText(R.id.desc, spancontenttext)
        expandedNotif.setTextViewText(R.id.title, etTitle.text.toString())
        expandedNotif.setTextViewText(R.id.desc, spancontenttext)

        val notif = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getString(R.string.iaf))
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setOngoing(true)
            .setAutoCancel(false)

        if(selectedFontSize < 3 && spancontenttext.length < 49){
            notif.setCustomContentView(normalNotif)
        }else if(selectedFontSize == 0 && spancontenttext.length < 130){
            notif.setCustomContentView(normalNotif)
        }else{
            notif.setCustomBigContentView(expandedNotif)
        }

        if(imgFile != null){
            val myBitmap = BitmapFactory.decodeFile(imgFile?.path)
            notif.setCustomBigContentView(expandedNotif)
                .setLargeIcon(myBitmap)
            expandedNotif.setImageViewBitmap(R.id.ivImg, myBitmap)
        }
        with(NotificationManagerCompat.from(this)){
            notify(id, notif.build())
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
    private fun hasPermissionWriteExternalStorage():Boolean{
        return EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        Log.d("EasyPermission", "OnPermission granted $requestCode: ${perms.size}")
    }
    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)){
            AppSettingsDialog.Builder(this).build().show()
        }
    }
}