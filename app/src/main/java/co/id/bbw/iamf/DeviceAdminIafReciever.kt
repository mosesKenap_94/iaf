package co.id.bbw.iamf

import android.app.admin.DeviceAdminReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import co.id.bbw.iamf.utils.PostProvisioningHelper

class DeviceAdminIafReciever: DeviceAdminReceiver(){

    companion object{

        fun getComponent(context: Context):ComponentName{
            return ComponentName(context.applicationContext,DeviceAdminIafReciever::class.java)
        }
    }

    override fun onProfileProvisioningComplete(context: Context, intent: Intent) {
        val helper = PostProvisioningHelper(context)
        Log.e("DeviceAdminIafReciever", "go here")
        if(!helper.isDone()){
            val workprofile = Intent(context, EnableWorkProfileActivity::class.java)
            workprofile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(workprofile)
        }
    }

    private fun msg(context: Context, msg:String){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

}