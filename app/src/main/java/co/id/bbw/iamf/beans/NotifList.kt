package co.id.bbw.iamf.beans

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class NotifList:RealmObject() {
    @PrimaryKey
    var idNotif:Int?=null
    var title:String?=null
    var desc:String?=null
    var descFontSize:Int?=null
    var descFontColor:Int?=null
    var descFontType:Int?=null
    var descFontStyle:Int?=null
    var img:String?=null
    var isNotPeriodic:Boolean?=null
    var duration:Int?=null
}