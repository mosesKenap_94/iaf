package co.id.bbw.iamf

import android.app.*
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RemoteViews
import android.widget.TextView
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.id.bbw.iamf.beans.NotifList
import co.id.bbw.iamf.databinding.ActivityNotificationlistBinding
import co.id.bbw.iamf.databinding.ItemDataNotifBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_notificationlist.*

class NotificationListClass:BaseActivity() {


    private var adapter:MyAdapter?=null

    private lateinit var binding:ActivityNotificationlistBinding
    private lateinit var notificationManager:NotificationManager

    override fun bindingView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notificationlist)
    }
    override fun setLayout(): Int = R.layout.activity_notificationlist

    override fun onResume() {
        super.onResume()
        load()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notificationManager =  getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            channel.lightColor = Color.GREEN
            channel.importance = NotificationManager.IMPORTANCE_HIGH
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(channel)
        }

        adapter = MyAdapter()
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, 1))
        recycler.setHasFixedSize(true)

        createNew.setOnClickListener {
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        lockApp.setOnClickListener {
            finishAffinity()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add_notif, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.add){
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }else if(item.itemId == R.id.clear){
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.clear_all))
                .setMessage(getString(R.string.clear_all_text))
                .setNeutralButton(getString(R.string.cancel)){dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(getString(R.string.clear_action)){dialog, _ ->
                    dialog.dismiss()
                    notificationManager.cancelAll()
                }
                .create()
                .show()
        }else if(item.itemId == R.id.wipeWorkerProfile){
            MaterialAlertDialogBuilder(this)
                .setTitle("Turn Of Worker Profile")
                .setMessage("If you turn off this, user allow to edit App Settings IAMF application.")
                .setNeutralButton(getString(R.string.cancel)){dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton("Yes"){dialog, _ ->
                    dialog.dismiss()
                    removeProfile()
                }
                .create()
                .show()
        }
        return true
    }

    private fun removeProfile() {
        val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        dpm.wipeData(0)
    }

    fun load(){

        val results = realm.where(NotifList::class.java).findAll()
        adapter?.setData(results)
        recycler?.adapter=adapter

        if(adapter?.itemCount!! > 0){
            lyCreate.visibility = View.GONE
        }else{
            lyCreate.visibility = View.VISIBLE
        }

    }

    inner class MyAdapter:RecyclerView.Adapter<MyAdapter.VHAdapter>(){

        private var list:RealmResults<NotifList>?=null
        private val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        inner class VHAdapter(private val binding:ItemDataNotifBinding):RecyclerView.ViewHolder(binding.root){
            fun bind(arg:NotifList?){
                binding.data = arg
                val textSizeVal = when(arg?.descFontSize){
                    1-> 14f
                    2-> 16f
                    3-> 18f
                    4-> 20f
                    5-> 24f
                    else -> 12f
                }
                binding.desc.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeVal)
                binding.popup.setOnClickListener {
                    MaterialAlertDialogBuilder(itemView.context)
                        .setMessage("Do you want to remove this one?")
                        .setPositiveButton("Delete"){dialog,_ ->
                            dialog.dismiss()
                            notificationManager.cancel(arg?.idNotif?:0)
                            val pendingIntent = PendingIntent.getBroadcast(
                                applicationContext,
                                arg?.idNotif?:0,
                                Intent(applicationContext, BootRecieverNotif::class.java),
                                PendingIntent.FLAG_UPDATE_CURRENT)
                            alarmManager.cancel(pendingIntent)
                            pendingIntent.cancel()

                            val data = realm.where(NotifList::class.java).equalTo("idNotif", arg?.idNotif).findAll()
                            realm.executeTransaction {
                                data.deleteAllFromRealm()
                            }

                            load()
                        }
                        .setNegativeButton("Cancel"){dialog,_ -> dialog.dismiss()}
                        .create().show()
                }

                val typefaceFontStyle = when(arg?.descFontStyle){
                    1-> ResourcesCompat.getFont(itemView.context, R.font.times_new_roman)
                    2 -> ResourcesCompat.getFont(itemView.context, R.font.calibri)
                    else -> Typeface.DEFAULT
                }

                val typefaceStyle = when(arg?.descFontType){
                    1 -> Typeface.BOLD
                    2-> Typeface.ITALIC
                    else -> Typeface.NORMAL
                }
                binding.desc.setTypeface(typefaceFontStyle, typefaceStyle)
                binding.ivImage.setImageBitmap(BitmapFactory.decodeFile(arg?.img))

            }
            init {
                itemView.setOnClickListener {
                    val dialog =Dialog(itemView.context)
                    dialog.setContentView(R.layout.dialog_option)
                    val call = dialog.findViewById<TextView>(R.id.call)
                    val edit = dialog.findViewById<TextView>(R.id.edit)
                    val hide = dialog.findViewById<LinearLayout>(R.id.hide)
                    edit.setOnClickListener {
                        dialog.dismiss()
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.putExtra("id", list!![adapterPosition]!!.idNotif?:0)
                        intent.putExtra("edit", true)

                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    }
                    call.setOnClickListener {
                        dialog.dismiss()
                        callOneTimeNotif(
                            list!![adapterPosition]!!.idNotif?:0,
                            list!![adapterPosition]!!.title?:"",
                            list!![adapterPosition]!!.desc?:"",
                            list!![adapterPosition]!!.descFontSize?:0,
                            list!![adapterPosition]!!.descFontStyle?:0,
                            list!![adapterPosition]!!.descFontType?:0,
                            list!![adapterPosition]!!.img?:"",
                            itemView.context
                        )
                    }

                    hide.setOnClickListener {
                        dialog.dismiss()

                        notificationManager.cancel(list!![adapterPosition]!!.idNotif?:0)
                        val pendingIntent = PendingIntent.getBroadcast(
                            applicationContext,
                            list!![adapterPosition]!!.idNotif?:0,
                            Intent(applicationContext, BootRecieverNotif::class.java),
                            PendingIntent.FLAG_UPDATE_CURRENT)
                        alarmManager.cancel(pendingIntent)
                        pendingIntent.cancel()
                    }
                    dialog.show()
                }
            }
        }

        private fun callOneTimeNotif(
            id:Int,
            title:String,
            msg:String,
            selectedFontSize:Int,
            selectedFontStyle:Int,
            selectedFontType:Int,
            imgPath:String,
            context:Context) {

            val normalNotif = RemoteViews(packageName, R.layout.custom_notification)
            val expandedNotif = RemoteViews(packageName, R.layout.custom_notification_expanded)

            val text = if(msg.isNotEmpty()) msg else "No message"
            val spancontenttext = SpannableString(text)
            val textSizeVal = when(selectedFontSize){
                1-> 14
                2-> 16
                3-> 18
                4-> 20
                5-> 24
                else -> 12
            }
            val typefaceFontStyle = when(selectedFontStyle){
                1-> ResourcesCompat.getFont(context, R.font.times_new_roman)
                2 -> ResourcesCompat.getFont(context, R.font.calibri)
                else -> Typeface.DEFAULT
            }
            val typefaceStyle = when(selectedFontType){
                1 -> Typeface.BOLD
                2-> Typeface.ITALIC
                else -> Typeface.NORMAL
            }
            spancontenttext.setSpan(AbsoluteSizeSpan(textSizeVal, true), 0, msg.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            spancontenttext.setSpan(StyleSpan(typefaceFontStyle!!.style), 0, msg.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spancontenttext.setSpan(StyleSpan(typefaceStyle), 0, msg.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            normalNotif.setTextViewText(R.id.title, title)
            normalNotif.setTextViewText(R.id.desc, spancontenttext)
            expandedNotif.setTextViewText(R.id.title, title)
            expandedNotif.setTextViewText(R.id.desc, spancontenttext)

            val notif = NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
//                .setCustomContentView(normalNotif)
//                .setCustomBigContentView(expandedNotif)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setOngoing(true)
                .setAutoCancel(false)

            if(selectedFontSize < 3 && spancontenttext.length < 49){
                notif.setCustomContentView(normalNotif)
            }else if(selectedFontSize == 0 && spancontenttext.length < 130){
                notif.setCustomContentView(normalNotif)
            }else{
                notif.setCustomBigContentView(expandedNotif)
            }

            if(imgPath.isNotEmpty()){
                val myBitmap = BitmapFactory.decodeFile(imgPath)
                notif.setCustomBigContentView(expandedNotif)
                    .setLargeIcon(myBitmap)
                expandedNotif.setImageViewBitmap(R.id.ivImg, myBitmap)
            }

            Log.e("notif create", "$id")
            with(NotificationManagerCompat.from(context)){
                notify(id, notif.build())
            }

        }

        fun setData(list:RealmResults<NotifList>){
            this.list = list
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHAdapter {
            return VHAdapter((DataBindingUtil.inflate(layoutInflater, R.layout.item_data_notif, parent, false)))
        }

        override fun onBindViewHolder(holder: VHAdapter, position: Int) {
            holder.bind(list!![position])
        }

        override fun getItemCount(): Int {
            return list?.size?:0
        }
    }
}