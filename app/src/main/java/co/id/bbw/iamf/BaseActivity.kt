package co.id.bbw.iamf

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm

abstract class BaseActivity:AppCompatActivity(), BaseUI {

    override val ctx: Context?
        get() = this
    override val realm: Realm
        get() = mRealm

    private lateinit var mRealm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRealm = Realm.getDefaultInstance()

        bindingView()
    }

    protected open fun bindingView(){
        setContentView(setLayout())
    }

    abstract fun setLayout():Int
}