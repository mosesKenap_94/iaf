package co.id.bbw.iamf

import android.content.Context
import io.realm.Realm

interface BaseUI {
    val ctx:Context?
    val realm:Realm
}