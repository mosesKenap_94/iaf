package co.id.bbw.iamf

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.UserManager
import co.id.bbw.iamf.utils.PostProvisioningHelper
import kotlinx.android.synthetic.main.activity_enable_work_profile.*

class EnableWorkProfileActivity:BaseActivity() {

    override fun setLayout(): Int {
        return R.layout.activity_enable_work_profile
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val provisioningHelper = PostProvisioningHelper(this)
        if(!provisioningHelper.isDone()){
            provisioningHelper.completeProvisioning()
        }
        val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        dpm.addUserRestriction(componentName, UserManager.DISALLOW_APPS_CONTROL)

        btnOkay.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}