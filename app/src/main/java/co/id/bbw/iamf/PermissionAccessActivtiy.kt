package co.id.bbw.iamf

import android.app.Activity
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.UserManager
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_permission_access.*

const val OPEN_LOCK = "Iaf_0420#"

class PermissionAccessActivtiy:AppCompatActivity() {

    private lateinit var manager:DevicePolicyManager
    private lateinit var userManager: UserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission_access)
        manager = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        userManager = getSystemService(Context.USER_SERVICE) as UserManager

//        removeProfile()
//        Log.e("worker profile", "status: ${manager.isProfileOwnerApp(packageName)}")
//        if(!manager.isProfileOwnerApp(packageName)) {
//            provisionManageProfile()
//        }


        submit.setOnClickListener {//ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
//            val setting = Intent(Settings.Secure.)
//            startActivity(setting)

            submit()
        }

        if(BuildConfig.DEBUG){
            password.setText(OPEN_LOCK)
            confirmPass.setText(OPEN_LOCK)
        }
    }

    private fun removeProfile() {
        val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        dpm.wipeData(0)
    }

    private fun provisionManageProfile(){
        val intent = Intent(DevicePolicyManager.ACTION_PROVISION_MANAGED_PROFILE)
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            intent.putExtra(DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME, packageName)
        }else{
            val component = ComponentName(this, DeviceAdminIafReciever::class.java)
            intent.putExtra(
                DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME,
                component
            )
        }

        if (intent.resolveActivity(packageManager) != null) {
            Toast.makeText(this, "provisionManageProfile success", Toast.LENGTH_SHORT).show()
            startActivityForResult(intent, 1)
            finish()
        } else {
            Toast.makeText(this, "Device provisioning is not enabled. Stopping.",
                Toast.LENGTH_SHORT).show();
        }
    }

    private fun submit() {

        if(password.text!!.isEmpty()){
            password.error = "Please fill the blank"
            return
        }

        if(confirmPass.text!!.isEmpty()){
            confirmPass.error = "Please fill the blank"
            return
        }

        val _setPass = password.text!!.toString()
        val _setConfPass = confirmPass.text!!.toString()

        if(_setPass == _setConfPass){
            if(_setPass == OPEN_LOCK && _setConfPass == OPEN_LOCK){
                startActivity(Intent(this, NotificationListClass::class.java))
                finish()
            }else{
                Toast.makeText(this, "Password incorrect!", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Password not match!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this, "Provisioning Done", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Provisioning Failed", Toast.LENGTH_SHORT).show()
            }
        }
    }


}