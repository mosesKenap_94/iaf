package co.id.bbw.iamf

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.res.ResourcesCompat
import java.util.*

class BootRecieverNotif: BroadcastReceiver() {

    val CHANNEL_ID ="IAF"
    val CHANNEL_NAME = "IAF_MESSAGE"

    override fun onReceive(context: Context?, intent: Intent?) {
        if("android.intent.action.BOOT_COMPLETED" == intent?.action){
            callNotif(context!!, intent)
        }else{
            callNotif(context!!, intent!!)
        }
    }

    private fun callNotif(context: Context, intent: Intent?) {
        val msg = intent?.getStringExtra("message")
        val title = intent?.getStringExtra("title")
        val imgFile = intent?.getStringExtra("img")
        val duration = intent?.getIntExtra("duration", 0)
        val idNotif = intent?.getIntExtra("idNotif", 0)
        val selectedFontType = intent?.getIntExtra("fonttype", 0)
        val selectedFontSize = intent?.getIntExtra("fontsize", 0)
        val selectedFontStyle = intent?.getIntExtra("fontstyle", 0)

        val normalNotif = RemoteViews(context.packageName, R.layout.custom_notification)
        val expandedNotif = RemoteViews(context.packageName, R.layout.custom_notification_expanded)

        val spancontenttext = SpannableString(msg?:"")
        val textSizeVal = when(selectedFontSize){
            1-> 14
            2-> 16
            3-> 18
            4-> 20
            5-> 24
            else -> 12
        }
        val typefaceFontStyle = when(selectedFontStyle){
            1-> ResourcesCompat.getFont(context, R.font.times_new_roman)
            2 -> ResourcesCompat.getFont(context, R.font.calibri)
            else -> Typeface.DEFAULT
        }
        val typefaceStyle = when(selectedFontType){
            1 -> Typeface.BOLD
            2-> Typeface.ITALIC
            else -> Typeface.NORMAL
        }
        spancontenttext.setSpan(AbsoluteSizeSpan(textSizeVal, true), 0, msg?.length?:0, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        spancontenttext.setSpan(StyleSpan(typefaceFontStyle!!.style), 0, msg?.length?:0, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spancontenttext.setSpan(StyleSpan(typefaceStyle), 0, msg?.length?:0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        normalNotif.setTextViewText(R.id.title, title)
        normalNotif.setTextViewText(R.id.desc, spancontenttext)
        expandedNotif.setTextViewText(R.id.title, title)
        expandedNotif.setTextViewText(R.id.desc, spancontenttext)

        val notificationManager =context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            channel.lightColor = Color.GREEN
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            channel.importance = NotificationManager.IMPORTANCE_HIGH
            notificationManager.createNotificationChannel(channel)
        }

        val notif = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle(context.getString(R.string.iaf))
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setOngoing(true)
            .setAutoCancel(false)

        if((textSizeVal == 12 || textSizeVal ==14 || textSizeVal == 16)  && spancontenttext.length < 49){
            notif.setCustomContentView(normalNotif)
        }else if(textSizeVal == 12 && spancontenttext.length < 130){
            notif.setCustomContentView(normalNotif)
        }else{
            notif.setCustomBigContentView(expandedNotif)
        }


        if(imgFile != null){
            val bitmap = BitmapFactory.decodeFile(imgFile)
            notif.setCustomBigContentView(expandedNotif)
                .setLargeIcon(bitmap)
            expandedNotif.setImageViewBitmap(R.id.ivImg, bitmap)
        }

        with(NotificationManagerCompat.from(context)){
            notify(idNotif?:0, notif.build())
        }

        //extend alarm
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val extendIntent = Intent(context, BootRecieverNotif::class.java)
        extendIntent.putExtra("duration", duration)
        extendIntent.putExtra("message", msg)
        extendIntent.putExtra("idNotif", idNotif)

        val pendingIntent = PendingIntent.getBroadcast(context, idNotif?:0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val calendar = Calendar.getInstance(Locale("id", "ID"))
        calendar.set(Calendar.MILLISECOND, duration?:30000)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
        }
    }
}