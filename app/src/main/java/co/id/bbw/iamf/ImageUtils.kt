package co.id.bbw.iamf

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import id.zelory.compressor.Compressor
import java.io.File
import java.text.DecimalFormat
import java.util.*
import kotlin.math.log10
import kotlin.math.pow

class ImageUtils {

    companion object{

        //for SDK < 29
        val PATH_STORAGE_IMG =String.format(
            Locale.ENGLISH, "%s/Iaf", Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM).absolutePath)

        fun compressImage(context: Context, imgFile: File): File {
            return  Compressor(context)
                .setQuality(60)
                .setCompressFormat(Bitmap.CompressFormat.WEBP)
                .setDestinationDirectoryPath(PATH_STORAGE_IMG)
                .compressToFile(imgFile)
        }

        fun getRealUriPath(context: Context, uri: Uri):String{
            var result = ""
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = context.contentResolver.query(uri,proj,null,null,null,null)
            cursor!!.moveToFirst()
            val id = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(id?:0)
            cursor.close()

            return result
        }


        fun getReadableFileSize(size:Long):String{
            if(size <= 0){
                return "0"
            }

            val units = arrayOf("B", "KB", "MB", "GB")
            val digitGroups = (log10(size.toDouble()) / log10(1024.0)).toInt()
            return DecimalFormat("#,##0.#").format(size/1024.0.pow(digitGroups.toDouble())) + " " + units[digitGroups]
        }

        fun isSizeLessThan_2MB(size:Long):Boolean{
            val maxSize = 2000f
            val fileSize = size/1024.0
            return fileSize < maxSize
        }

    }
}